package reporter

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
)

var authLogsBlacklist = []string{
	"crond[",
}

var userRegexStr = `(for( user)?|user) ([^\s]+)`
var logTypeRegexStr = hostName + `\s+([a-zA-Z]+)`

// Auth forwards log output from auth.log to Elastic
func (r Reporter) Auth(elasticURL string, logLine string) {
	if LogInBlacklist(authLogsBlacklist, logLine) {
		return
	}

	epochLineTime := GetUnixTimestamp(logLine)

	// Get the log type
	logTypeRegex, _ := regexp.Compile(logTypeRegexStr)
	authLogType := ""
	if matches := logTypeRegex.FindStringSubmatch(logLine); matches != nil && len(matches) > 1 {
		authLogType = matches[1]
	}

	// Just in case there was a parsing problem on the date, make sure it's above 0
	if epochLineTime > 0 && authLogType != "" {
		connectionType := getAuthConnectionType(logLine)
		userName, ipAddress := getAuthUserInfo(logLine)
		data := make(map[string]string)
		data["@timestamp"] = fmt.Sprintf("%v", epochLineTime)
		data["log_type"] = "auth"
		data["auth_type"] = authLogType
		data["user"] = userName
		data["info"] = logLine

		if authLogType == "sshd" {
			data["connection_type"] = connectionType
			data["ip_address"] = ipAddress
		}

		jsonData, _ := json.Marshal(data)
		r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
	}
}

func getAuthUserInfo(logLine string) (userName string, ipAddress string) {
	ipRegex, _ := regexp.Compile(ipRegexStr)
	userRegex, _ := regexp.Compile(userRegexStr)

	ipAddress = ipRegex.FindString(logLine)
	if matches := userRegex.FindAllStringSubmatch(logLine, -1); matches != nil && len(matches) > 0 {
		userName = matches[0][len(matches[0])-1]
	}

	return userName, ipAddress
}

func getAuthConnectionType(logLine string) string {
	connectionType := "Misc"

	if strings.Contains(logLine, "Invalid") {
		connectionType = "Invalid User"
	} else if strings.Contains(logLine, "Received disconnect") {
		connectionType = "Received Disconnect"
	} else if strings.Contains(logLine, "Disconnected") {
		connectionType = "Disconnected"
	} else if strings.Contains(logLine, "Accepted") {
		connectionType = "Accepted"
	}

	return connectionType
}
