package main

import (
	"bufio"
	"fmt"
	"os"
	"reporter"
	"strings"
)

var elasticURL = "http://localhost:9200"
var logReaderLogFolder = "/var/log/logreader"
var verbose = false // In case one wants to log extra details

const logConfigFile = "/etc/logreader.conf"

var logsToWatch = make([]string, 0)
var readyToTail = make(chan int, 1) // To start tailing the files
var readPipe, writePipe, _ = os.Pipe()

func main() {
	// root or empty user (probably a boot service for Linux)
	if currentUser := os.Getenv("USER"); currentUser != "root" && currentUser != "" {
		fmt.Println("WARNING: This program may not work unless you are root or have root privelages!")
	}

	logsToWatch := ParseConfig()

	configureLogFolder()
	filterLogsThatDoNotExist()
	go reporter.PushMissedLogs(elasticURL)
	go TailLogs(logsToWatch)
	startLogListener()
}

func configureLogFolder() {
	if _, err := os.Stat(logReaderLogFolder); err != nil {
		os.Mkdir(logReaderLogFolder, 0755)
	}
	reporter.SetLogsFileFolder(logReaderLogFolder, verbose)
}

func filterLogsThatDoNotExist() {
	filteredLogs := make([]string, 0)
	for _, log := range logsToWatch {
		if _, err := os.Stat(log); err != nil {
			reporter.WriteToLogFile(fmt.Sprintf("Could not find %s - does it exist?\n", log))
			continue
		}

		filteredLogs = append(filteredLogs, log)
	}

	logsToWatch = filteredLogs
}

func startLogListener() {
	pipeScanner := bufio.NewScanner(readPipe)
	currentLog := ""

	// Only one log to watch? Set it as the default currentLog
	if len(logsToWatch) == 1 {
		currentLog = logsToWatch[0]
	}

	readyToTail <- 0 // Initiate tailing!
	for pipeScanner.Scan() {
		scannedText := pipeScanner.Text()

		// New log found from the tail
		if strings.HasPrefix(scannedText, "==>") {
			currentLog = strings.Split(scannedText, " ")[1]
			continue
		}

		// Skip odd empty lines
		if scannedText == "" {
			continue
		}

		ParseLogDetails(currentLog, scannedText, verbose)
	}
}
