package reporter

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"
)

var grafanaTimeRegexStr = `\d{4,4}-\d{2,2}-\d{2,2}T\d{2,2}:\d{2,2}:\d{2,2}Z`
var grafanaUserRegexStr = `(User|uname)=([^\s]+)`

// Grafana forwards log output from grafana.log to Elastic
func (r Reporter) Grafana(elasticURL string, logLine string) {
	// Can't use the normal GetUnixTimestamp since it's formatted differently here
	logLine = strings.Replace(logLine, "+0000", "Z", -1) // Fix to match RFC3339
	grafanaTimeRegex, _ := regexp.Compile(grafanaTimeRegexStr)
	lineDate, _ := time.ParseInLocation(time.RFC3339, grafanaTimeRegex.FindString(logLine), time.UTC)
	epochTime := lineDate.Unix()

	if epochTime > 0 {
		connectionType := getGrafanaConnectionType(logLine)
		user, ipAddress := getGrafanaUserInfo(logLine)

		data := make(map[string]string)
		data["@timestamp"] = fmt.Sprintf("%v", epochTime)
		data["log_type"] = "grafana"
		data["info"] = logLine
		data["connection_type"] = connectionType
		data["user"] = user

		if ipAddress != "" {
			data["ip_address"] = ipAddress
		}

		jsonData, _ := json.Marshal(data)
		r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
	}
}

func getGrafanaUserInfo(logLine string) (userName string, ip string) {
	user, ipAddress := "", ""
	grafanaUserRegex, _ := regexp.Compile(grafanaUserRegexStr)
	if matches := grafanaUserRegex.FindAllStringSubmatch(logLine, -1); matches != nil && len(matches) > 0 {
		user = matches[0][len(matches[0])-1]
	}

	ipRegex, _ := regexp.Compile(ipRegexStr)
	ipAddress = ipRegex.FindString(logLine)

	return user, ipAddress
}

func getGrafanaConnectionType(logLine string) string {
	connectionType := "Misc"

	if strings.Contains(logLine, "Login") {
		connectionType = "Login"
	} else if strings.Contains(logLine, "Logout") {
		connectionType = "Logout"
	} else if strings.Contains(logLine, "Invalid") {
		connectionType = "Invalid"
	} else if strings.Contains(logLine, "Completed") {
		connectionType = "Request Completed"
	}

	return connectionType
}
