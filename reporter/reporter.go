package reporter

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

// Reporter is a struct object
type Reporter struct{}

const timeout = 2 // How many seconds before timing out
const varLogEndpoint = "/varlog/logs"

var missedLogsFile = "logreader_missed_logs.log"
var logreaderLogFile = "logreader.log"

// Any int sent to it will indicate it's empty
var bufferEmpty = make(chan int, 1)

// Logs print out the hostname sometimes
var hostName, _ = os.Hostname()

// Common regex patterns
var dateRegexStr = `^[a-zA-Z]+\s+\d+\s+[\d:]+`
var ipRegexStr = `[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}`
var verbose = false

// SetLogsFileFolder sets a custom folder to place the missedLogsFile
func SetLogsFileFolder(folderName string, verboseEnabled bool) {
	missedLogsFile = fmt.Sprintf("%s/%s", folderName, missedLogsFile)
	logreaderLogFile = fmt.Sprintf("%s/%s", folderName, logreaderLogFile)
	verbose = verboseEnabled
}

// WriteToLogFile writes to the logreader's own log file
func WriteToLogFile(text string) {
	f, err := os.OpenFile(logreaderLogFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err == nil {
		f.WriteString(text)
		f.Close()
	}
}

func (r Reporter) sendElasticData(elasticFullURL string, jsonData []byte) {
	response, err := http.Post(elasticFullURL, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		WriteToLogFile("Error in POST call. Waiting on availability...\n")
		go pushBufferToElastic(elasticFullURL, jsonData)
	} else {
		defer response.Body.Close()
		bufferEmpty <- 0
	}

	select {
	case <-bufferEmpty:
		if verbose {
			WriteToLogFile(fmt.Sprintf("OK: %s\n", string(jsonData)))
		}
		return
	case <-time.After(timeout * time.Second):
		WriteToLogFile("ERROR: Timed out on waiting for Elastic\n")
		savedJSON := make(map[string]string)
		savedJSON["fullURL"] = elasticFullURL
		savedJSON["data"] = string(jsonData)
		savedJSONData, _ := json.Marshal(savedJSON)

		f, err := os.OpenFile(missedLogsFile, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
		if err != nil {
			WriteToLogFile(fmt.Sprintf("ERROR: Could not create or open a %s file. Logs lost\n", missedLogsFile))
		}
		f.WriteString(fmt.Sprintln(string(savedJSONData)))
		f.Close()
		WriteToLogFile(fmt.Sprintf("Wrote missed jsonData to %s\n", missedLogsFile))
	}
}

// PushMissedLogs will push any json data/logs in the missedLogsFile to Elastic
func PushMissedLogs(elasticURL string) {
	_, err := os.Stat(missedLogsFile)
	if err != nil {
		WriteToLogFile(fmt.Sprintf("%s doesn't exist or has wrong permissions. Skipping missed logs.\n", missedLogsFile))
		return
	}

	elasticUp := make(chan int, 1) // One hit means it's up

	response, err := http.Get(elasticURL)

	if err != nil {
		WriteToLogFile(fmt.Sprintf("Elastic appears to be down. Waiting %v seconds to try again...\n", timeout-1))
		go func() {
			time.Sleep((timeout - 1) * time.Second)
			response, err := http.Get(elasticURL)
			if err == nil {
				response.Body.Close()
				WriteToLogFile(fmt.Sprintf("Elastic's up!\n"))
				elasticUp <- 0
			}
		}()
	} else {
		defer response.Body.Close()
		elasticUp <- 0
	}

	select {
	case <-elasticUp:
		f, err := os.OpenFile(missedLogsFile, os.O_RDONLY, 0666)
		if err != nil {
			WriteToLogFile(fmt.Sprintf("File open error! %s\n", err))
			return
		}
		defer f.Close()
		defer os.Remove(missedLogsFile)

		fScanner := bufio.NewScanner(f)
		for fScanner.Scan() {
			dataObject := make(map[string]string)
			json.Unmarshal(fScanner.Bytes(), &dataObject)
			fullURL := dataObject["fullURL"]
			jsonData := ([]byte)(dataObject["data"])

			// Invalid line or something went wrong
			if dataObject["data"] == "" || fullURL == "" {
				continue
			}

			response, err := http.Post(fullURL, "application/json", bytes.NewBuffer(jsonData))
			if err != nil {
				WriteToLogFile(fmt.Sprintf("Error sending %s to %s:\n", jsonData, fullURL))
				continue
			}
			defer response.Body.Close()
			WriteToLogFile(".")
		}
		WriteToLogFile(fmt.Sprintf("Done! Deleting %s\n", missedLogsFile))
	case <-time.After(timeout * time.Second):
		WriteToLogFile("ERROR: Timed out on waiting for Elastic. Not writing missing logs this time.\n")
	}
}

func pushBufferToElastic(elasticFullURL string, jsonData []byte) {
	for {
		time.Sleep(5 * time.Second)
		response, err := http.Post(elasticFullURL, "application/json", bytes.NewBuffer(jsonData))
		if err == nil {
			bufferEmpty <- 0
			response.Body.Close()
			return
		}
	}
}

// GetUnixTimestamp gets an Epoch timestamp from a common Unix syslogd log
func GetUnixTimestamp(logLine string) int64 {
	dateRegex, _ := regexp.Compile(dateRegexStr)
	lineDate, _ := time.ParseInLocation(time.Stamp, dateRegex.FindString(logLine), time.Local)
	return lineDate.AddDate(time.Now().Year(), 0, 0).Unix()
}

// LogInWhitelist returns true if the log contains any of the strings in whitelist
func LogInWhitelist(whitelist []string, logLine string) bool {
	for _, v := range whitelist {
		if strings.Contains(strings.ToLower(logLine), strings.ToLower(v)) {
			return true
		}
	}
	return false
}

// LogInBlacklist returns true if the log contains any of the strings in blacklist
func LogInBlacklist(blacklist []string, logLine string) bool {
	for _, v := range blacklist {
		if strings.Contains(strings.ToLower(logLine), strings.ToLower(v)) {
			return true
		}
	}
	return false
}
