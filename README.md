# What is this?

This is simply a log reader that I've created for my personal server. However, it's pretty flexible
and should allow anyone to pull it and adjust it to their own needs. I've created a few files that
I use to log into my ElasticSearch database. Seeing how these are set up should allow anyone to
create their own *.go files for their own logs as they please. 

# Requirements

Since this was made for my system, it's got some specific requirements to it:

1. /etc/init.d directory
2. rc-update installed and using that /etc/init.d directory
3. docker installed (init.d script uses this - remove the requirement if you use elastic externally)

# How do I use this?

## Option 1: from source

First, make sure that ElasticSearch is installed and has a mapping for `/varlogs`. Then place the 
logreader/ and reporter/ directories into your $GOPATH and from the logreader folder, run 
`sudo go run *.go`.

## Option 2: from the binary

First, make sure that ElasticSearch is installed and has a mapping for `/varlogs`. Then run the
binary as you would with any other executable on Linux (i.e. `./logreader`).

## Option 3: full installer

Download the latest tar.gz version located in [tags](https://gitlab.com/mcgaritystuff/logreader/-/tags).
Then uncompress that with `tar xzvf logreader.tar.gz`. Change into the directory and run `./install.sh`.
This will install the init.d script, add the service to rc-update, and then start the logreader. Feel
free to first update `logreader.conf` first before running the installer if you want to specify the logs.
**Note: You must restart the service any time you change `logreader.conf`!**