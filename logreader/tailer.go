package main

import (
	"os/exec"
)

// TailLogs reads the logs in /etc/logreader.conf and calls a function to send the output to writePipe
func TailLogs(logsToWatch []string) {
	if logsToWatch != nil && len(logsToWatch) > 0 {
		tailLogs(logsToWatch)
	}
}

func tailLogs(logsToWatch []string) {
	tailArgs := []string{"-n", "0", "-F"}
	tailArgs = append(tailArgs, logsToWatch...)

	<-readyToTail // Wait until it's ready to tail the logs
	cmd := exec.Command("tail", tailArgs...)
	cmd.Stdout = writePipe
	cmd.Run()
}
