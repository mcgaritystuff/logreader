package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"reporter"
	"strings"
)

// ParseLogDetails will read the string of the given log line and
// send it to Elastic if it matches one of the reporters in the
// reporters folder. If Elastic is not available, it'll store it
// until Elastic becomes available.
func ParseLogDetails(currentLog string, logLine string, verbose bool) {
	// Get the file name without the extensions or dots
	logFileName := filepath.Base(currentLog)
	logFileName = strings.Split(logFileName, ".")[0]

	// Now call the respective method on the reporter through reflection
	methodName := strings.Title(logFileName)
	args := make([]reflect.Value, 2)
	args[0] = reflect.ValueOf(elasticURL)
	args[1] = reflect.ValueOf(logLine)
	respectiveMethod := reflect.ValueOf(reporter.Reporter{}).MethodByName(methodName)

	if !respectiveMethod.IsValid() {
		reporter.WriteToLogFile(fmt.Sprintf("It appears there is no reporter method called %s\n", methodName))
	} else {
		if verbose {
			reporter.WriteToLogFile(fmt.Sprintf("Got log for %s\n", methodName))
		}
		go respectiveMethod.Call(args)
	}
}

// ParseConfig parses the /etc/logreader.conf for variables and
// returns []string with the log file paths
func ParseConfig() []string {
	configFile, err := os.Open(logConfigFile)
	if err != nil {
		reporter.WriteToLogFile(fmt.Sprintf("Error opening file %s: %s\n", logConfigFile, err.Error()))
		os.Exit(1)
	}

	configFileScanner := bufio.NewScanner(configFile)
	for configFileScanner.Scan() {
		resultText := configFileScanner.Text()
		if resultText == "" || strings.HasPrefix(resultText, "#") {
			continue
		}
		splitResults := strings.Split(resultText, "=")
		// Must follow: key=value
		if len(splitResults) > 2 {
			reporter.WriteToLogFile(fmt.Sprintf("Config file improperly set: %s\n", resultText))
		}

		switch splitResults[0] {
		case "elastic_url":
			elasticURL = splitResults[1]
		case "log":
			logsToWatch = append(logsToWatch, splitResults[1])
		case "logreader_log_folder":
			if _, err := os.Stat(splitResults[1]); err != nil {
				if splitResults[1] != logReaderLogFolder {
					reporter.WriteToLogFile(fmt.Sprintf("Could not find folder %s for logs. Please create it\n", splitResults[1]))
				}
				break
			}
			logReaderLogFolder = splitResults[1]
		case "verbose_logging":
			if strings.ToLower(splitResults[1]) == "true" {
				verbose = true
			}
		case "ip_to_countries_file":
			reporter.WriteToLogFile("Scanning IP/Country file ...")
			countryfile, err := os.Open(splitResults[1])
			if err != nil {
				reporter.WriteToLogFile(fmt.Sprintf("Error opening file %s: %s\n", splitResults[1], err.Error()))
				os.Exit(1)
			}
			countryFileScanner := bufio.NewScanner(countryfile)
			for countryFileScanner.Scan() {
				countryLine := countryFileScanner.Text()
				splitLine := strings.Split(countryLine, " ")
				if len(splitLine) != 2 {
					reporter.WriteToLogFile("Error - The following line was incorrectly formatted in the country file: " + countryLine)
					continue
				}
				reporter.CountryIPs[splitLine[0]] = splitLine[1]
			}
			reporter.WriteToLogFile("Done")
		}
	}

	return logsToWatch
}
