package reporter

import (
	"encoding/json"
	"fmt"
)

var nullmailerWhitelist = []string{
	"smtp:",
}

// Nullmailer forwards log output from nullmailer.log to Elastic
func (r Reporter) Nullmailer(elasticURL string, logLine string) {
	if !LogInWhitelist(nullmailerWhitelist, logLine) {
		return
	}

	epochLineTime := GetUnixTimestamp(logLine)

	if epochLineTime > 0 {
		data := make(map[string]string)
		data["@timestamp"] = fmt.Sprintf("%v", epochLineTime)
		data["log_type"] = "mail"
		data["info"] = logLine

		jsonData, _ := json.Marshal(data)
		r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
	}
}
