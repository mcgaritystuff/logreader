package reporter

import (
	"encoding/json"
	"fmt"
	"regexp"
)

var speedRegexStr = `(1[0]+) [KMG]bps`
var kernWhitelist = []string{
	"tg3",
}

// Kern forwards log output from kern.log to Elastic
func (r Reporter) Kern(elasticURL string, logLine string) {
	if !LogInWhitelist(kernWhitelist, logLine) {
		return
	}

	epochLineTime := GetUnixTimestamp(logLine)

	if epochLineTime > 0 {
		linkSpeed := ""

		speedRegex, _ := regexp.Compile(speedRegexStr)
		if matches := speedRegex.FindStringSubmatch(logLine); matches != nil && len(matches) > 1 {
			linkSpeed = matches[1]
		}

		data := make(map[string]string)
		data["@timestamp"] = fmt.Sprintf("%v", epochLineTime)
		data["log_type"] = "kern"
		data["info"] = logLine
		if linkSpeed != "" {
			data["link_speed"] = linkSpeed
		}

		jsonData, _ := json.Marshal(data)
		r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
	}
}
