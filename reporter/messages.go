package reporter

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"strconv"
)

var messagesLogsWhitelist = []string{
	"filterlog[",
	"suricata[",
	"rt-ac86u-7638 kernel:",
}

var firewallLogIndices = map[string]int {
	"interface": 4,
	"action": 6,
	"direction": 7,
	"protocol": 16,
	"src_ip": 18,
	"dst_ip": 19,
	"src_port": 20,
	"dst_port": 21,
}

var routerLogIndices = map[string]int {
	"action": 5,
	"mac_address": 8,
	"src_ip": 9,
	"dst_ip": 10,
	"protocol": 17,
	"src_port": 18,
	"dst_port": 19,
}

var CountryIPs = map[string]string{}

// Messages forwards log output from /var/log/messages to Elastic
func (r Reporter) Messages(elasticURL string, logLine string) {
	if !LogInWhitelist(messagesLogsWhitelist, logLine) {
		return
	}

	// 1st check if firewall
	// 2nd check if suricata
	// last, assume router
	if strings.Contains(strings.ToLower(logLine), strings.ToLower(messagesLogsWhitelist[0])) {
		r.handleFirewallMessages(elasticURL, logLine)
	} else if strings.Contains(strings.ToLower(logLine), strings.ToLower(messagesLogsWhitelist[1])) {
		r.handleSuricataMessages(elasticURL, logLine)
	} else {
		r.handleRouterMessages(elasticURL, logLine)
	}
}

func (r Reporter) handleFirewallMessages(elasticURL string, logLine string) {
	data := make(map[string]string)
	logSections := strings.Split(logLine, ",")
	// Only parse logs that have enough to index a log
	if len(logSections) < 22 {
		return
	}
	
	epochLineTime := GetUnixTimestamp(logLine)

	data["@timestamp"] = fmt.Sprintf("%v", epochLineTime)
	data["log_type"] = "firewall"
	
	data["interface"] = logSections[firewallLogIndices["interface"]]
	data["action"] = logSections[firewallLogIndices["action"]]
	data["direction"] = logSections[firewallLogIndices["direction"]]
	data["protocol"] = logSections[firewallLogIndices["protocol"]]
	data["src_ip"] = logSections[firewallLogIndices["src_ip"]]
	data["dst_ip"] = logSections[firewallLogIndices["dst_ip"]]
	data["src_port"] = logSections[firewallLogIndices["src_port"]]
	data["dst_port"] = logSections[firewallLogIndices["dst_port"]]
	data["src_country"] = getCountry(data["src_ip"])
	data["dst_country"] = getCountry(data["dst_ip"])
	data["info"] = logLine

	jsonData, _ := json.Marshal(data)
	r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
}

func (r Reporter) handleSuricataMessages(elasticURL string, logLine string) {
	data := make(map[string]string)

	// Only worry about Drop logs
	if !strings.Contains(strings.ToLower(logLine), "[drop]") {
		return
	}
	
	epochLineTime := GetUnixTimestamp(logLine)

	// Not entirely sure of all the possible logs, so just log the entire info line
	data["@timestamp"] = fmt.Sprintf("%v", epochLineTime)
	data["log_type"] = "suricata"
	data["info"] = logLine

	jsonData, _ := json.Marshal(data)
	r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
}

func (r Reporter) handleRouterMessages(elasticURL string, logLine string) {
	data := make(map[string]string)
	equalsRemovalRegex := regexp.MustCompile("[A-Z]+=")
	logSections := strings.Fields(equalsRemovalRegex.ReplaceAllString(logLine, ""))
	// Only parse logs that have enough to index a log
	if len(logSections) < 20 {
		return
	}
	
	epochLineTime := GetUnixTimestamp(logLine)

	data["@timestamp"] = fmt.Sprintf("%v", epochLineTime)
	data["log_type"] = "router_traffic"
	
	data["mac_address"] = logSections[routerLogIndices["mac_address"]]
	data["action"] = strings.ToLower(logSections[routerLogIndices["action"]])
	data["protocol"] = logSections[routerLogIndices["protocol"]]
	data["src_ip"] = logSections[routerLogIndices["src_ip"]]
	data["dst_ip"] = logSections[routerLogIndices["dst_ip"]]
	data["src_port"] = logSections[routerLogIndices["src_port"]]
	data["dst_port"] = logSections[routerLogIndices["dst_port"]]
	data["src_country"] = getCountry(data["src_ip"])
	data["dst_country"] = getCountry(data["dst_ip"])
	data["info"] = logLine

	jsonData, _ := json.Marshal(data)
	r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
}

func getCountry(ipAddress string) string {
	// Get each digit for the IP address to start figuring out which
	// subnet it's in
	splitDigits := strings.Split(ipAddress, ".")
	var convertedDigits []int
	for _,digit := range splitDigits {
		nextDigit, _ := strconv.Atoi(digit)
		convertedDigits = append(convertedDigits, nextDigit)
	} 

	// Something weird happen, safe fail
	if len(convertedDigits) < 4 {
		return "Unknown"
	}
	
	// If it's a private IP, just return "PrivateIP"
	if (convertedDigits[0] == 10) || 
		 (convertedDigits[0] == 172 && convertedDigits[1] >= 16 && convertedDigits[1] <= 31) ||
		 (convertedDigits[0] == 192 && convertedDigits[1] == 168) {
			 return "PrivateIP"
	}

	// Now we need to find the corresponding subnet IP range
	// to determine the country it belongs to.
	// An IPv4 is 32 bits long, so loop through it 32 times.
	for i:=31; i>=0; i-- {
		// We need to check all subnets from 32 to this one, in case the owners
		// have a weird ownership range
		for j:=31; j>=i; j-- {

			// Check if the one before this current loop cycle exists so we can stop
			prevIPSubnet := fmt.Sprintf("%v.%v.%v.%v/%v", convertedDigits[0], convertedDigits[1], convertedDigits[2], convertedDigits[3], j+1)

			if CountryIPs[prevIPSubnet] != "" {
				return CountryIPs[prevIPSubnet]
			}
		}
		// Get the current IP digit index (0-3)
		currentDigitIndex := i/8
		// Figure out how much padding is needed to handle the subnet
		padding := 8 - (i%8)
		// Convert this digit to the next subnet mask
		newDigit := (convertedDigits[currentDigitIndex] >> padding) << padding
		// Save it
		convertedDigits[currentDigitIndex] = newDigit
	}

	// At this point, none of the subnets worked, so time to check for generic ones
	currentIP := ipAddress
	if CountryIPs[currentIP] != "" {
		return CountryIPs[currentIP]
	}
	// Change to x.x.x.0 and try again
	currentIP = strings.Replace(ipAddress, ipAddress, fmt.Sprintf("%v.%v.%v.0/24", splitDigits[0], splitDigits[1], splitDigits[2]), 1)
	if CountryIPs[currentIP] != "" {
		return CountryIPs[currentIP]
	}
	// Change to x.x.0.0 and try again
	currentIP = strings.Replace(ipAddress, ipAddress, fmt.Sprintf("%v.%v.0.0/16", splitDigits[0], splitDigits[1]), 1)
	if CountryIPs[currentIP] != "" {
		return CountryIPs[currentIP]
	}
	// Change to x.0.0.0 and try again
	currentIP = strings.Replace(ipAddress, ipAddress, fmt.Sprintf("%v.0.0.0/8", splitDigits[0]), 1)
	if CountryIPs[currentIP] != "" {
		return CountryIPs[currentIP]
	}

	// At this point, we hit all 0s on the IP. Return Unknown since it's not saved
	return "Unknown"
}