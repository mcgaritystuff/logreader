#!/bin/bash

if [ "$EUID" -ne 0 ]; then 
  echo "Please run as root"
  exit
fi

if [ -f /etc/logreader.conf ]; then
  echo "/etc/logreader.conf exists! Not overwriting!"
else
  echo "Moving config file to /etc/logreader.conf"
  mv logreader.conf.template /etc/logreader.conf
fi;

echo "Setting executables and moving to logreader to /usr/bin"
chmod +x *
mv logreader /usr/bin/logreader

echo "Installing latest rc cript in /etc/init.d"
mv logreader.initd /etc/init.d/logreader

if [[ $(rc-update show | grep logreader) == "" ]]; then
  echo "Adding to rc-update boot"
  rc-update add logreader boot
fi;

if [[ $(/etc/init.d/logreader status) =~ "stopped" ]]; then
  echo "Starting the service"
  /etc/init.d/logreader start
else
  echo "Restarting the service"
  /etc/init.d/logreader restart
fi;

echo "Done!"