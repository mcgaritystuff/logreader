package reporter

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"
)

var nextcloudWhitelist = []string{
	"login failed",
}

var nextcloudUserRegexStr = `failed: ([^\(]+) \(Remote`

// Nextcloud forwards log output from nextcloud.log to Elastic
func (r Reporter) Nextcloud(elasticURL string, logLine string) {
	if !LogInWhitelist(nextcloudWhitelist, logLine) {
		return
	}

	// Nextcloud logs are in json
	parsedLogLine := make(map[string]string)
	json.Unmarshal(([]byte)(logLine), &parsedLogLine)

	if len(parsedLogLine) > 0 {
		user := ""
		// Can't use the normal GetUnixTimestamp since it's formatted differently here
		// Some reason, nextcloud uses + instead of Z
		timeStr := strings.Replace(parsedLogLine["time"], "+00:00", "Z00:00", -1)
		timeStr = strings.Replace(timeStr, "+0000", "Z00:00", -1)
		lineDate, _ := time.ParseInLocation(time.RFC3339, timeStr, time.UTC)
		epochTime := lineDate.Unix()

		nextCloudUserRegex, _ := regexp.Compile(nextcloudUserRegexStr)
		if matches := nextCloudUserRegex.FindStringSubmatch(parsedLogLine["message"]); matches != nil && len(matches) > 1 {
			user = matches[1]
		}

		data := make(map[string]string)
		data["@timestamp"] = fmt.Sprintf("%v", epochTime)
		data["log_type"] = "nextcloud"
		data["info"] = logLine
		data["connection_type"] = "Login Fail"
		data["user"] = user

		if parsedLogLine["remoteAddr"] != "" {
			data["ip_address"] = parsedLogLine["remoteAddr"]
		}

		jsonData, _ := json.Marshal(data)
		r.sendElasticData(elasticURL+varLogEndpoint, jsonData)
	}
}
